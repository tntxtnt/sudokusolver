#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <vector>
#include <queue>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "SudokuBoard.h"

#define IN ,
#define FROM ,

typedef std::vector<std::string>          GridValues;
typedef std::vector<std::vector<size_t> > GridPeers;
typedef std::vector<std::vector<size_t> > GridGroups;

GridPeers getPeers();
GridGroups getGroups();
GridValues parseGrid(const std::string&);
std::string randomGrid(int=17);
void display(const GridValues&, bool=true);
void display(const GridPeers&);
void printCell(size_t);
void printChars(char, size_t);
bool reduce(GridValues&, size_t=std::string::npos);
bool found(char, const std::string&);
bool erase(char, std::string&);
GridValues dfs(const GridValues&);
int solve(const std::string&, bool=true);
bool isValidSudoku(const GridValues&);

const GridPeers  PEERS  = getPeers();  //20 peers
const GridGroups GROUPS = getGroups(); //27 groups (best=23)


int main()
{
    /*std::ifstream fin("sudoku.txt");
    std::string title, line, grid;
    int sum = 0;
    while (std::getline(fin, title))
    {
        //std::cout << title << "\n";
        grid.clear();
        for (int i = 0; i < 9; ++i)
        {
            std::getline(fin, line);
            grid += line;
        }
        sum += solve(grid, 0);
        //std::cin.get();
    }
    fin.close();
    std::cout << sum << "\n";

    fin.open("top95.txt");
    while (std::getline(fin, grid))
        solve(grid, 0);
    fin.close();

    fin.open("hardest.txt");
    while (std::getline(fin, grid))
        solve(grid, 0);
    fin.close();

    */

    /*grid = ". . . |. . 5 |. 8 . "
           ". . . |6 . 1 |. 4 3 "
           ". . . |. . . |. . . "
           "------+------+------"
           ". 1 . |5 . . |. . . "
           ". . . |1 . 6 |. . . "
           "3 . . |. . . |. . 5 "
           "------+------+------"
           "5 3 . |. . . |. 6 1 "
           ". . . |. . . |. . 4 "
           ". . . |. . . |. . . ";
    solve(grid); //320sec */

    /*grid = ".....6....59.....82....8....45........3."
           ".......6..3.54...325..6..................";
    solve(grid); //0.8sec
    */
    std::string grid;
    /*grid = "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ..."
           "... ... ...";*/
    /*grid = "8.. ... ..."
           "..3 6.. ..."
           ".7. .9. 2.."
           ".5. ..7 ..."
           "... .45 7.."
           "... 1.. .3."
           "..1 ... .68"
           "..8 5.. .1."
           ".9. ... 4..";*/
    grid = ".82 ... ..."
           "7.5 ... 21."
           ".9. ..6 ..3"
           "41. .35 ..7"
           "..6 .7. 9.."
           "... 2.. ..."
           "... ... ..."
           ".37 ..1 ..."
           ".5. .68 794";
    //solve(grid); //0.8sec -> 0.1s

    SudokuBoard board(grid);
    std::cout << "\n";
    board.print();
    if (board.solve()) board.print();



    //17 1406558739 557 ~132s
    //18 1406561567 733 ~300s
    //17 1406562030  26 ~80s
    //17 1406562895 515 ~???
    /*time_t seed = time(0);
    std::cout << "seed = " << seed << "\n";
    int stop = 1000;
    seed = seed;
    srand(seed);
    for (int i = 0; i < stop; ++i)
    {
        std::cout << "i = " << std::setw(3) << i << " ";
        solve(randomGrid(17), 0);
        if (i % 10 == 9) std::cout << "\n";
    }*/
}


int solve(const std::string& grid, bool show)
{
    GridValues values = parseGrid(grid);

    if (show) display(values, false);

    reduce(values);
    values = dfs(values);

    if (show) display(values);

    std::cout << isValidSudoku(values) << "\n";

    return (values[0][0]-'0')*100
         + (values[1][0]-'0')*10
         + (values[2][0]-'0');
}

GridValues dfs(const GridValues& values)
{
    size_t minId = values.size();
    size_t minlen = std::string::npos;
    for (size_t i = 1; i < values.size(); ++i)
        if (values[i].size() < minlen && values[i].size() != 1)
            minId = i, minlen = values[i].size();
    // Solved!
    if (minId == values.size())
        return values;

    for (size_t i = 0; i < values[minId].size(); ++i)
    {
        GridValues g = values;
        g[minId] = values[minId].substr(i, 1);
        if (reduce(g, minId)) {
            GridValues gnext = dfs(g);
            if (!gnext.empty())
                return gnext;
        }
    }
    return GridValues();
}

bool isValidSudoku(const GridValues& values)
{
    if (values.empty()) return false;
    for (size_t i = 0; i < GROUPS.size(); ++i)
    {
        std::vector<int> counter(9, 0);
        for (size_t j = 0; j < GROUPS[i].size(); ++j)
        {
            size_t p = GROUPS[i][j];
            if (values[p].size() == 1)
                counter[values[p][0] - 49]++;
        }
        for (int c = 0; c < 9; ++c)
            if (counter[c] > 1)
                return false;
    }
    return true;
}

bool reduce(GridValues& values, size_t at)
{
    std::queue<size_t> q;
    if (at != std::string::npos) q.push(at);
    else {
        for (size_t i = 0; i < values.size(); ++i)
            if (values[i].size() == 1)
                q.push(i);
    }
    while (!q.empty())
    {
        size_t id = q.front();
        char c = values[id][0];
        q.pop();
        // (1)
        for (size_t i = 0; i < PEERS[id].size(); ++i)
        {
            size_t p = PEERS[id][i];
            if (erase(c FROM values[p])) {
                if (values[p].empty())     return false;
                if (values[p].size() == 1) q.push(p);
            }
        }
        // (2)
        for (size_t i = 0; i < GROUPS.size(); ++i)
        {
            std::vector<size_t> dplaces;
            dplaces.reserve(9);
            for (size_t j = 0; j < GROUPS[i].size(); ++j)
            {
                size_t p = GROUPS[i][j];
                if (found(c IN values[p]))
                    dplaces.push_back(p);
            }
            if (dplaces.empty())
                return false;
            if (dplaces.size() == 1 && values[dplaces[0]].size() != 1) {
                q.push(dplaces[0]);
                values[dplaces[0]] = values[id];
            }
        }
    }
    return true;
}

bool found(char c, const std::string& s)
{
    //return *std::lower_bound(s.begin(), s.end(), c) == c;
    return s.find(c) != s.npos;
}

bool erase(char c, std::string& s)
{
    /*std::string::iterator it = std::lower_bound(s.begin(), s.end(), c);
    if (*it == c) {
        s.erase(it);
        return true;
    }
    return false;*/
    size_t pos = s.find(c);
    if (pos != s.npos) {
        //s.erase(pos, 1);
        std::swap(s[pos], s[s.size()-1]);
        s.erase(s.size()-1);
        return true;
    }
    return false;
}

void printCell(size_t id)
{
    std::cout << char(id/9+'A') << id%9 + 1;
}

GridGroups getGroups()
{
    GridGroups groups(27);
    // rows
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i].push_back(9*i + j);
    // columns
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i+9].push_back(i + 9*j);
    // boxes
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i+18].push_back(i/3*27 + i%3*3 + j/3*9 + j%3);
    return groups;
}

GridPeers getPeers()
{
    GridPeers peers(81);
    for (size_t i = 0; i < 81; ++i)
    {
        size_t row = i/9;
        size_t col = i%9;
        size_t box = row/3*3 + col/3;
        for (size_t j = 0; j < 81; ++j)
        {
            if (j == i) continue;
            size_t jrow = j/9;
            size_t jcol = j%9;
            size_t jbox = jrow/3*3 + jcol/3;
            if (jrow == row || jcol == col || jbox == box)
                peers[i].push_back(j);
        }
    }
    return peers;
}

std::string randomGrid(int count)
{
    GridValues values(81, "123456789");
    int n = 0;
    while (n < count)
    {
        size_t i = rand() % 81;
        values[i][0] = rand() % 9 + 1 + '0';
        values[i].resize(1);
        if (!isValidSudoku(values))
            values[i] = "123456789";
        else ++n;
    }
    std::string grid;
    for (size_t i = 0; i < values.size(); ++i)
        grid += values[i].size() == 1 ? values[i][0] : '.';
    return grid;
}

GridValues parseGrid(const std::string& grid)
{
    GridValues values;
    for (size_t i = 0; i < grid.size(); ++i)
    {
        if (grid[i] == '0' || grid[i] == '.')
            values.push_back("123456789");
        else if (isdigit(grid[i]))
            values.push_back(grid.substr(i,1));
    }
    return values;
}

void printChars(char c, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        std::cout << c;
}

void display(const GridValues& values, bool detailed)
{
    size_t maxlen = 1;
    for (size_t i = 0; i < values.size(); ++i)
        if (values[i].size() > maxlen)
            maxlen = values[i].size();

    if (!detailed) maxlen = 1;

    for (size_t i = 0; i < values.size(); ++i)
    {
        size_t lspaces = (maxlen - values[i].size()) / 2;
        size_t rspaces = maxlen - values[i].size() - lspaces;
        if (detailed) {
            printChars(' ', lspaces+1);
            std::cout << values[i];
            printChars(' ', rspaces+1);
        } else {
            std::cout << ' ';
            if (values[i].size() == 1)
                std::cout << values[i];
            else
                std::cout << '.';
            std::cout << ' ';
        }
        if (i%9 == 8) std::cout << '\n';
        else if (i%3 == 2) std::cout << '|';
        if (i == 26 || i == 53) {
            printChars('-', (maxlen+2) * 3);
            std::cout << '+';
            printChars('-', (maxlen+2) * 3);
            std::cout << '+';
            printChars('-', (maxlen+2) * 3);
            std::cout << '\n';
        }
    }
    std::cout << '\n';
}

void display(const GridPeers& PEERS)
{
    for (size_t i = 0; i < PEERS.size(); ++i)
    {
        printCell(i);
        std::cout << ": ";
        for (size_t j = 0; j < PEERS[i].size(); ++j)
        {
            printCell(PEERS[i][j]);
            std::cout << ' ';
        }
        std::cout << '\n';
    }
}
