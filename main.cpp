#include "SudokuBoard.h"

int main()
{
    std::string grid =
        ".82 ... ..."
        "7.5 ... 21."
        ".9. ..6 ..3"
        "41. .35 ..7"
        "..6 .7. 9.."
        "... 2.. ..."
        "... ... ..."
        ".37 ..1 ..."
        ".5. .68 794";
    SudokuBoard board(grid);
    board.print();
    if (board.solve()) board.print();
}
