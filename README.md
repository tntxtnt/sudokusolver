### Sudoku Solver
 - Based on [Peter Norvig's sudoku solver](http://norvig.com/sudoku.html)
 - Algorithm: assign all certain values (reduce), then use DFS to solve other possible values.
 - Example output:
 
         .  8  2 | .  .  . | .  .  .
         7  .  5 | .  .  . | 2  1  .
         .  9  . | .  .  6 | .  .  3
        ---------+---------+---------
         4  1  . | .  3  5 | .  .  7
         .  .  6 | .  7  . | 9  .  .
         .  .  . | 2  .  . | .  .  .
        ---------+---------+---------
         .  .  . | .  .  . | .  .  .
         .  3  7 | .  .  1 | .  .  .
         .  5  . | .  6  8 | 7  9  4
        
         3  8  2 | 5  1  7 | 4  6  9
         7  6  5 | 9  4  3 | 2  1  8
         1  9  4 | 8  2  6 | 5  7  3
        ---------+---------+---------
         4  1  9 | 6  3  5 | 8  2  7
         8  2  6 | 1  7  4 | 9  3  5
         5  7  3 | 2  8  9 | 1  4  6
        ---------+---------+---------
         6  4  8 | 7  9  2 | 3  5  1
         9  3  7 | 4  5  1 | 6  8  2
         2  5  1 | 3  6  8 | 7  9  4
 