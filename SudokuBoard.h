#ifndef SUDOKUBOARD_H
#define SUDOKUBOARD_H

#include <iostream>
#include <string>
#include <vector>
#include <queue>

class SudokuBoard
{
typedef std::vector<std::string>       GridValues;
typedef std::vector<std::vector<int> > GridPeers;
typedef std::vector<std::vector<int> > GridGroups;

public: //methods
    SudokuBoard(const std::string& = "");
    bool parse(const std::string&);
    bool valid()const;
    bool solve();
    void print(bool = false);
    bool isAssigned(int)const;
    int  getCellValue(int)const;
private: // constants
    static const GridPeers  PEERS;  //20 peers each cell of total 81 cells
    static const GridGroups GROUPS; //27 groups (best=23)
private: // methods
    static GridPeers  getPeers();
    static GridGroups getGroups();
    static bool       found(char, const std::string&);
    static bool       erase(char, std::string&);
    static bool       reduce(GridValues&, int = -1);
    static GridValues dfs(const GridValues&);
    static void       printChars(char, size_t);
    void              parseGrid(const std::string&);
private: //members
    GridValues values;
};

#endif // SUDOKUBOARD_H
