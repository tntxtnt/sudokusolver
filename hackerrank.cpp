#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <iomanip>
#include <cctype>
#include <queue>
#include <fstream>
#include <algorithm>

class SudokuBoard
{
typedef std::vector<std::string>       GridValues;
typedef std::vector<std::vector<int> > GridPeers;
typedef std::vector<std::vector<int> > GridGroups;

public: //methods
    SudokuBoard(const std::string& = "");
    bool parse(const std::string&);
    bool valid()const;
    bool solve();
    void print(bool = false);
    bool isAssigned(int)const;
    int  getCellValue(int)const;
private: // constants
    static const GridPeers  PEERS;  //20 peers each cell of total 81 cells
    static const GridGroups GROUPS; //27 groups (best=23)
private: // methods
    static GridPeers  getPeers();
    static GridGroups getGroups();
    static bool       found(char, const std::string&);
    static bool       erase(char, std::string&);
    static bool       reduce(GridValues&, int = -1);
    static GridValues dfs(const GridValues&);
    static void       printChars(char, size_t);
    void              parseGrid(const std::string&);
private: //members
    GridValues values;
};

const SudokuBoard::GridPeers  SudokuBoard::PEERS  = SudokuBoard::getPeers();
const SudokuBoard::GridGroups SudokuBoard::GROUPS = SudokuBoard::getGroups();

int main()
{
    std::string grid;
    std::string line;
    while (std::cin >> line) grid += line;

    SudokuBoard board(grid);
    if (board.solve()) board.print();
}


SudokuBoard::SudokuBoard(const std::string& grid)
{
    parseGrid(grid);
}

SudokuBoard::GridPeers SudokuBoard::getPeers()
{
    GridPeers peers(81);
    for (size_t i = 0; i < 81; ++i)
    {
        size_t row = i/9;
        size_t col = i%9;
        size_t box = row/3*3 + col/3;
        for (size_t j = 0; j < 81; ++j)
        {
            if (j == i) continue;
            size_t jrow = j/9;
            size_t jcol = j%9;
            size_t jbox = jrow/3*3 + jcol/3;
            if (jrow == row || jcol == col || jbox == box)
                peers[i].push_back(j);
        }
    }
    return peers;
}

SudokuBoard::GridGroups SudokuBoard::getGroups()
{
    GridGroups groups(27);
    // rows
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i].push_back(9*i + j);
    // columns
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i+9].push_back(i + 9*j);
    // boxes
    for (size_t i = 0; i < 9; ++i)
        for (size_t j = 0; j < 9; ++j)
            groups[i+18].push_back(i/3*27 + i%3*3 + j/3*9 + j%3);
    return groups;
}

void SudokuBoard::parseGrid(const std::string& grid)
{
    values.resize(81, "123456789");
    for (size_t i = 0, j = 0; i < grid.size() && j < 81; ++i)
    {
        if (grid[i] == '0' || grid[i] == '.')
            j++;
        else if (grid[i] >= '1' && grid[i] <= '9')
            values[j++] = grid.substr(i,1);
    }
}

bool SudokuBoard::isAssigned(int id)const
{
    return values[id].length() == 1;
}

int SudokuBoard::getCellValue(int id)const
{
    if (!isAssigned(id)) return 0;
    return values[id][0] - '0';
}

bool SudokuBoard::valid()const
{
    if (values.empty()) return false;
    for (size_t i = 0; i < GROUPS.size(); ++i)
    {
        std::vector<int> count(9, 0);
        for (size_t j = 0; j < GROUPS[i].size(); ++j)
        {
            size_t cell = GROUPS[i][j];
            if (isAssigned(cell)) //only check assigned values
                ++count[getCellValue(cell)-1];
        }
        for (int c = 0; c < 9; ++c)
            if (count[c] > 1)
                return false;
    }
    return true;
}

bool SudokuBoard::parse(const std::string& grid)
{
    parseGrid(grid);
    return valid();
}

bool SudokuBoard::found(char c, const std::string& s)
{
    return s.find(c) != s.npos;
}

bool SudokuBoard::erase(char c, std::string& s)
{
    size_t pos = s.find(c);
    if (pos != s.npos) {
        std::swap(s[pos], s[s.size()-1]);
        s.erase(s.size()-1);
        return true;
    }
    return false;
}

bool SudokuBoard::reduce(GridValues& values, int at)
{
    std::queue<int> q;
    if (at != -1) q.push(at);
    else {
        for (size_t i = 0; i < values.size(); ++i)
            if (values[i].size() == 1)
                q.push(i);
    }
    while (!q.empty())
    {
        int id = q.front();
        char c = values[id][0];
        q.pop();
        // (1)
        for (size_t i = 0; i < PEERS[id].size(); ++i)
        {
            int p = PEERS[id][i];
            if (erase(c, values[p])) {
                if (values[p].empty())     return false;
                if (values[p].size() == 1) q.push(p);
            }
        }
        // (2)
        for (size_t i = 0; i < GROUPS.size(); ++i)
        {
            std::vector<int> dplaces;
            dplaces.reserve(9);
            for (size_t j = 0; j < GROUPS[i].size(); ++j)
            {
                int cell = GROUPS[i][j];
                if (found(c, values[cell]))
                    dplaces.push_back(cell);
            }
            if (dplaces.empty())
                return false;
            if (dplaces.size() == 1 && values[dplaces[0]].size() != 1) {
                q.push(dplaces[0]);
                values[dplaces[0]] = values[id];
            }
        }
    }
    return true;
}

SudokuBoard::GridValues SudokuBoard::dfs(const GridValues& values)
{
    size_t minId = values.size();
    size_t minlen = 10;
    for (size_t i = 1; i < values.size(); ++i)
        if (values[i].size() < minlen && values[i].size() != 1)
            minId = i, minlen = values[i].size();
    // Solved!
    if (minId == values.size())
        return values;

    for (size_t i = 0; i < values[minId].size(); ++i)
    {
        GridValues g = values;
        g[minId] = values[minId].substr(i, 1);
        if (reduce(g, minId)) {
            GridValues gnext = dfs(g);
            if (!gnext.empty())
                return gnext;
        }
    }
    return GridValues();
}

bool SudokuBoard::solve()
{
    if (!reduce(values)) return false;
    values = dfs(values);
    return valid();
}

void SudokuBoard::printChars(char c, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        std::cout << c;
}

void SudokuBoard::print(bool detailed)
{
    for (int i = 0; i < 9; ++i)
    {
        std::string line;
        for (int j = 0; j < 9; ++j)
            line += values[i*9 + j];
        std::cout << line << "\n";
    }
}